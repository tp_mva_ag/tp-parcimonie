import numpy as np
from scipy.signal import convolve2d
from scipy.io import loadmat
import matplotlib.pyplot as plt


def h2d_compute(h1d):
    return np.dot(h1d.T, h1d)


def convolution(image, h1d):
    """ This function computes the convolustion of an image with a 2D separation filter
    of the form h2d(x,y) = h1d(x)h1d(y)

    :param image:
    :param h1d:
    :return:
    """

    # We compute H2D
    h2d = h2d_compute(h1d)

    # We return the convolution
    # return np.convolve(image, h2d)
    return convolve2d(image, h2d, mode='same')


def wavelet_a_trous(image, h1d, nb_iterations, debug=False, plot=False):
    """ Implementation of the "à trous" algorithm
    to perform the forward isotropic wavelet transform

    :param plot:
    :param debug:
    :param image:
    :param h1d:
    :param nb_iterations:
    :return: last_c: the last C image
    :return: w_list: list of W images
    """

    # We create empty lists
    w_list = []

    last_c = np.array(image)

    for i in range(nb_iterations):
        # We create the new H1D filter, with 0 between values
        new_h1d = np.zeros((1, h1d.size * (i + 1) - i))
        for j in range(new_h1d.size):
            if j % (i + 1) == 0:
                new_h1d[0, j] = h1d[int(j / (i + 1))]

        if debug:
            print(new_h1d)

        new_c = convolution(last_c, new_h1d)

        w_list.append(last_c - new_c)
        last_c = new_c.copy()

        if plot:
            plt.imshow(new_c)

    return last_c, w_list


def backward_wavelet(last_c, w_list):
    """ Implementation of the backward isotropic wavelet tranform

    :param last_c:
    :param w_list:
    :return:
    """

    c = last_c.copy()

    for w in w_list:
        c += w

    return c


def main():
    print('Hello World')
    h1d_test = np.array([1 / 16, 1 / 4, 3 / 8, 1 / 4, 1 / 16])

    image = np.zeros((101, 101))
    image[50, 50] = 1

    c, w_list = wavelet_a_trous(image, h1d_test, 5)

    plt.figure()
    for i in range(len(w_list)):
        print('Mean value of W : %s' % np.mean(w_list[i]))
        plt.subplot(2, len(w_list), i + 1)
        plt.imshow(w_list[i])
    plt.subplot(153)
    plt.title('All wavelets scale for a Kronecker')
    # plt.show()

    image_rebuilt = backward_wavelet(c, w_list)

    plt.figure()
    plt.imshow(image_rebuilt)
    plt.title('Image rebuilt')

    ngc2997_mat = loadmat('ngc2997.mat')
    ngc2997 = ngc2997_mat['ngc2997']

    c2, w_list2 = wavelet_a_trous(ngc2997, h1d_test, 9)

    plt.figure()
    for i in range(len(w_list2)):
        print('Mean value of W : %s' % np.mean(w_list2[i]))
        plt.subplot(3, 3, i + 1)
        plt.imshow(w_list2[i])
    # plt.subplot(153)
    # plt.title('All wavelets scale from the image')

    image_rebuilt = backward_wavelet(c2, w_list2)

    plt.figure()
    plt.subplot(121)
    plt.imshow(ngc2997)
    plt.title('Original image')
    plt.subplot(122)
    plt.imshow(image_rebuilt)
    plt.title('Image rebuilt')

    np.random.normal()

    plt.show()
    return


if __name__ == '__main__':
    main()
