from TP1.func_TP1 import *
from tqdm import tqdm
from time import time

h1d_default = np.array([1 / 16, 1 / 4, 3 / 8, 1 / 4, 1 / 16])


def get_coeff(nb_iterations=5):
    """ Returns the std variations of the wavelets, needed later.

    :param nb_iterations:
    :return:
    """
    noise = np.random.normal(scale=1.0, size=(10, 10))

    c, w_list = wavelet_a_trous(noise, h1d_default, nb_iterations)

    coeff_list = []
    for w in w_list:
        coeff_list.append(np.std(w))

    return coeff_list


def soft_thrd(im, lam):
    """ Implements the soft thresholding on an image, with the coefficient lambda

    :param im: image
    :param lam: coeff
    :return im: thresholded image
    """
    im_mean = np.mean(im)

    im_c = np.array(im) - im_mean
    x = np.zeros(im_c.shape)

    im_sup = im_c - lam
    im_inf = im_c + lam

    x[im_sup > 0] = im_sup[im_sup > 0]
    x[im_inf < 0] = im_inf[im_inf < 0]

    return x + im_mean


def hard_thrd(im, lam):
    """ Implements the hard thresholding on an image, with the coefficient lambda

    :param im: image
    :param lam: coeff
    :return im: thresholded image
    """
    im_mean = np.mean(im)

    im_c = np.array(im) - im_mean
    x = im_c.copy()

    x = x * (np.abs(x) > np.sqrt(lam))

    return x + im_mean


def denois_thrd(image, thrd_func=soft_thrd, lam=0.1, nb_iterations=5):
    """ Denoise an image using thresholding methods. The soft or hard is choosen by giving the corresponding function.

    :param image: image to denoise
    :param thrd_func: thresholding function
    :param lam: lambda parameter
    :param nb_iterations: number of iterations of the wavelets transform
    :return image: denoised image
    """
    # On calcule la transformée en ondelette de l'image
    h1d = np.array([1 / 16, 1 / 4, 3 / 8, 1 / 4, 1 / 16])
    last_c, w_list = wavelet_a_trous(image, h1d, nb_iterations)

    # On applique sur les coeffs obtenus le soft threshold
    list_coeff = get_coeff(nb_iterations)
    w_list_thrd = []
    for idx, w in enumerate(w_list):
        w_list_thrd.append(thrd_func(w, lam * list_coeff[idx]))

    # On reconstruit l'image
    out_im = backward_wavelet(last_c, w_list_thrd)

    return out_im


def soft_denois(image, lam=0.1, nb_iterations=5):
    """ Call the denoising image with the soft thresholding

    :param image:
    :param lam:
    :param nb_iterations:
    :return:
    """
    return denois_thrd(image, soft_thrd, lam, nb_iterations)


def hard_denois(image, lam=0.1, nb_iterations=5):
    """ Call the denoising image with the hard thresholding

    :param image:
    :param lam:
    :param nb_iterations:
    :return:
    """
    return denois_thrd(image, hard_thrd, lam, nb_iterations)


def mse(im, im_orig):
    """ Compute the Normalised Mean Squared Error between two images

    :param im:
    :param im_orig:
    :return:
    """
    return np.linalg.norm(im_orig - im) / np.linalg.norm(im_orig)


def main_tp2_noise():
    """ Testing the denoising functions

    :return:
    """
    simu_sky_mat = loadmat('TP2/simu_sky.mat')
    simu_sky = simu_sky_mat['simu_sky']

    sigma = np.max(simu_sky) / 50.
    simu_sky_noise = simu_sky + np.random.normal(size=np.shape(simu_sky), scale=sigma)

    # Determine best k for Hard Thresholding
    list_mse_h = []
    list_k_h = np.linspace(0, 10000, 50)
    for k in tqdm(list_k_h, desc='Determining best k_hard'):
        list_mse_h.append(mse(simu_sky, hard_denois(simu_sky_noise, lam=k * sigma)))

    plt.figure()
    plt.plot(list_k_h, list_mse_h)
    plt.title('Determinng best k for hard thresholding')
    plt.xlabel('k')
    plt.ylabel('MSE')
    plt.show(block=False)

    # Determine best k for Soft Thresholding
    list_mse_s = []
    list_k_s = np.linspace(0, 5, 50)
    for k in tqdm(list_k_s, desc='Determining best k_soft'):
        list_mse_s.append(mse(simu_sky, soft_denois(simu_sky_noise, lam=k * sigma)))

    plt.figure()
    plt.plot(list_k_s, list_mse_s)
    plt.title('Determinng best k for soft thresholding')
    plt.xlabel('k')
    plt.ylabel('MSE')
    plt.show(block=False)

    # Computing best images
    k_soft = list_k_s[np.argmin(list_mse_s)]
    k_hard = list_k_h[np.argmin(list_mse_h)]

    image_rebuilt_soft = soft_denois(simu_sky_noise, lam=k_soft * sigma)
    image_rebuilt_hard = hard_denois(simu_sky_noise, lam=k_hard * sigma)

    # Plotting all
    plt.figure()
    plt.subplot(234)
    plt.imshow(simu_sky, cmap='gray')
    plt.title('Original image')
    plt.subplot(231)
    plt.imshow(simu_sky_noise, cmap='gray')
    plt.title('Noisy image\nMSE=%.3f' % mse(simu_sky_noise, simu_sky))
    plt.subplot(232)
    plt.imshow(image_rebuilt_soft, cmap='gray')
    plt.title('Soft Denoising\nMSE=%.3f' % mse(image_rebuilt_soft, simu_sky))
    plt.subplot(233)
    plt.imshow(image_rebuilt_hard, cmap='gray')
    plt.title('Hard Denoising\nMSE=%.3f' % mse(image_rebuilt_hard, simu_sky))

    plt.subplot(235)
    plt.imshow(simu_sky_noise - image_rebuilt_soft, cmap='gray')
    plt.title('Residual - Soft')
    plt.subplot(236)
    plt.imshow(simu_sky_noise - image_rebuilt_hard, cmap='gray')
    plt.title('Residual - Hard')

    plt.show()


def for_back_split(im, h, nb_iter, gamma, lam, h1d):
    """ Implementation of the Forward Backward Splitting algorithm for deblurring

    :param im:
    :param h:
    :param nb_iter:
    :param gamma:
    :param lam:
    :param h1d:
    :return:
    """
    c, list_w = wavelet_a_trous(im, h1d, 5)
    ht = np.conjugate(h)

    for _ in tqdm(range(nb_iter), desc='Iterating FBS'):

        psi_a = backward_wavelet(c, list_w)
        h_psi_a = convolve2d(h, psi_a, mode='same')
        diff = im - h_psi_a
        ht_diff = convolve2d(ht, diff, mode='same')
        c_grad, list_grad_w = wavelet_a_trous(ht_diff, h1d, 5)

        for i in range(len(list_w)):
            list_w[i] += gamma * list_grad_w[i]

        for i in range(len(list_w)):
            list_w[i] = soft_thrd(gamma * list_w[i], gamma * lam)

    x_est = backward_wavelet(c, list_w)
    return x_est


def for_back_split_inp(im, mask, nb_iter, gamma, lam, h1d):
    """ Implementation of the Forward Backward Splitting algorithm for inpainting

    :param mask:
    :param im:
    :param nb_iter:
    :param gamma:
    :param lam:
    :param h1d:
    :return:
    """
    c, list_w = wavelet_a_trous(im, h1d, 5)
    ht = np.conjugate(mask)

    for _ in tqdm(range(nb_iter), desc='Iterating FBS'):

        psi_a = backward_wavelet(c, list_w)
        h_psi_a = np.multiply(mask, psi_a)
        diff = im - h_psi_a
        ht_diff = np.multiply(ht, diff)
        c_grad, list_grad_w = wavelet_a_trous(ht_diff, h1d, 5)

        for i in range(len(list_w)):
            list_w[i] += gamma * list_grad_w[i]

        for i in range(len(list_w)):
            list_w[i] = soft_thrd(gamma * list_w[i], gamma * lam)

    x_est = backward_wavelet(c, list_w)
    return x_est


def main_tp2_blur():
    """ Test function for deblurring

    :return:
    """
    print('Loading mat')
    simu_sky_mat = loadmat('TP2/simu_sky.mat')
    simu_sky = simu_sky_mat['simu_sky']

    psf_mat = loadmat('TP2/simu_psf.mat')
    psf = psf_mat['simu_psf']

    print('Shape im = %s, Shape filter = %s' % (np.shape(simu_sky), np.shape(psf)))
    print('Convolving')

    start_time = time()
    simu_sky_blur = convolve2d(simu_sky, psf, mode='same')
    print('Convolution in %ss' % (time() - start_time))

    sigma = np.max(simu_sky) / 200.
    simu_sky_blur_noise = simu_sky_blur + np.random.normal(size=np.shape(simu_sky), scale=sigma)

    plt.figure()
    plt.imshow(simu_sky_blur_noise, cmap='gray')
    plt.title('Blurry and noisy image')
    plt.show(block=True)

    print('Starting FBS')

    gamma = float(1 / (2 * (np.linalg.norm(psf)**2)))
    lamb = 3 * sigma
    new_im = for_back_split(simu_sky_blur_noise, psf, 1, gamma, lamb, h1d_default)

    plt.figure()
    plt.imshow(new_im, cmap='gray')
    plt.title('Deblurred image')
    plt.show()


if __name__ == '__main__':
    main_tp2_blur()
