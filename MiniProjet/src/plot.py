import numpy as np
import matplotlib.pyplot as plt


def imshow_fft(fft, log=True, val_abs=True):
    if val_abs:
        fft = np.abs(fft)
    if log:
        fft = np.log(fft + 1e-16)
    plt.imshow(np.fft.fftshift(fft), cmap='gray')
    plt.axis('off')


def mask_b_1chan(masks, b, channel_nb):
    m_i = masks[:, :, channel_nb]
    b_i = b[:, :, channel_nb]

    plt.figure()
    plt.subplot(121)
    imshow_fft(m_i, log=False, val_abs=False)
    plt.title('Masque sur le canal %d\nNoir = Donnée manquante' %channel_nb)
    plt.axis('off')

    plt.subplot(122)
    imshow_fft(b_i)
    plt.title('Mesures B sur le canal %d (log)\nNoir = donnée manquante' % channel_nb)
    plt.axis('off')


def mask_10chan(masks):
    plt.figure()
    for i in range(10):
        plt.subplot(3, 4, i + 1)
        imshow_fft(masks[:, :, i], log=False, val_abs=False)
        plt.title("Canal #%d" % i)


def b_10chan(b):
    plt.figure()
    for i in range(10):
        plt.subplot(3, 4, i + 1)
        imshow_fft(b[:, :, i])
        plt.title("Canal #%d" % i)
        plt.axis('off')


def x_10chan(x):
    plt.figure()
    for i in range(10):
        plt.subplot(3, 4, i + 1)
        plt.imshow(x[:, :, i], cmap='gray')
        plt.title("Canal #%d" % i)
        plt.axis('off')


def n_10chan(n):
    plt.figure()
    for i in range(10):
        plt.subplot(3, 4, i + 1)
        plt.imshow(n[:, :, i], cmap='gray')
        plt.title("Canal #%d" % i)
        plt.axis('off')

    n_range = [n.min(), n.max()]
    plt.figure()
    for i in range(10):
        plt.subplot(3, 4, i + 1)
        plt.hist(n[:, :, i].flatten(), 100)
        plt.xlim(n_range)
        plt.title("Canal #%d" % i)


def sources_est_2(sources_est, title=''):
    plt.figure()
    plt.subplot(121)
    plt.imshow(sources_est[0], cmap='gray')
    plt.title(title + '\nEstimated source #1')

    plt.subplot(122)
    plt.imshow(sources_est[1], cmap='gray')
    plt.title(title + '\nEstimated source #2')
