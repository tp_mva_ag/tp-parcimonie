from scipy.io import loadmat
import matplotlib.pyplot as plt
import numpy as np
import MiniProjet.src.plot as plot

##

B = loadmat('MiniProjet/RadioInterferometry_MAT/Fourier_Measurements.mat')['B']
masks = loadmat('MiniProjet/RadioInterferometry_MAT/Fourier_Sampling.mat')['mask']

plot.mask_b_1chan(masks, B, 0)
plot.mask_b_1chan(masks, B, 5)

##

print('Proportions de données masquées par channel')

for i in range(10):
    print('\tChannel n#%d\t%d%% masquées' % (i, 100 * (1 - masks[i].mean())))

##

print('Masques, dans le domaine de Fourier')
plot.mask_10chan(masks)

print('Mesures, dans le domaine de Fourier')
plot.b_10chan(B)

##

x_n = np.zeros(shape=B.shape)

for i in range(10):
    # On calcule la transformée inverse pour chaque canal
    x_n[:, :, i] = np.fft.ifft2(B[:, :, i])

plot.x_10chan(x_n)

plt.figure()
plt.subplot(121)
plt.imshow(x_n[:, :, 0], cmap='gray')
plt.title('Canal #0')
plt.subplot(122)
plt.imshow(x_n[:, :, 1], cmap='gray')
plt.title('Canal #1')
plt.figure()
plt.subplot(121)
plt.imshow(x_n[:, :, 5], cmap='gray')
plt.title('Canal #5')
plt.subplot(122)
plt.imshow(x_n[:, :, 9], cmap='gray')
plt.title('Canal #9')

##

from TP3.pyBSS import perform_fast_ica

# Mise en forme de x pour les algorithmes

x = np.zeros(shape=(10, 256 ** 2))

for i in range(10):
    x[i] = x_n[:, :, i].flatten()

a_est, sources_est_star = perform_fast_ica(x, 2, do_whitening=True)
sources_est_star = np.reshape(sources_est_star, newshape=(2, 256, 256))

plt.figure()
plt.subplot(121)
plt.imshow(sources_est_star[0], cmap='gray')
plt.title('Estimated source #1')

plt.subplot(122)
plt.imshow(sources_est_star[1], cmap='gray')
plt.title('Estimated source #2')

plt.show()

##

N = loadmat('MiniProjet/RadioInterferometry_MAT/Noise_single_simulation.mat')['noise2']

plot.n_10chan(N)

plt.show()

##

from TP3.Starlet2D import starlet_forward_2d, starlet_backward_2d
from TP3.pyBSS import perform_gmca

j = 5
c_list = []
w_list = []

print('Forward Starlet transform')
for i in range(10):
    c, w = starlet_forward_2d(x_n[:, :, i], j=j)
    c_list.append(c)
    w_list.append(w)

len_c = 256 * 256
len_w = 256 * 256

print('Flattening data')
x_flat = np.zeros(shape=(10, len_c + j * len_w))
for i in range(10):
    c = c_list[i]
    w = w_list[i]
    x_flat[i][0:len_c] = c.flatten()

    w_transp = np.transpose(w, [2, 0, 1])
    print('Shape W = %s Shape W_tr = %s' % (w.shape, w_transp.shape))
    x_flat[i][len_c:] = w_transp.flatten()

print('GMCA')
a_est, sources_est_star, _ = perform_gmca(x_flat, 2)

print('Backward Starlets')
sources_est = np.zeros((2, 256, 256))

for i in range(2):
    source = sources_est_star[i]
    c = np.reshape(source[0:256 ** 2], (256, 256))

    w_transp = np.reshape(source[256 ** 2:], (5, 256, 256))
    w = np.transpose(w_transp, [1, 2, 0])
    print('Shape W_tr = %s Shape W = %s' % (w_transp.shape, w.shape))

    sources_est[i] = starlet_backward_2d(c, w)

plot.sources_est_2(sources_est)

import cv2

B_inpaint = B.copy()
for i in range(10):
    b_i = B[:, :, i]
    m_i = 1 - masks[:, :, i]

    b_abs = np.abs(b_i)
    b_phase = np.angle(b_i)

    b_abs = b_abs.astype(np.float32)
    b_phase = b_phase.astype(np.float32)

    b_abs_inp = cv2.inpaint(b_abs, m_i, 3, cv2.INPAINT_NS)
    b_phase_inp = cv2.inpaint(b_phase, m_i, 3, cv2.INPAINT_NS)

    B_inpaint[:, :, i] = b_abs_inp * np.exp(1j * b_phase_inp)
