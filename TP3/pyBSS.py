import numpy as np
import scipy.linalg as lng
import copy as cp
from sklearn.decomposition import FastICA


def mad(xin=0):
    """ Defines the median absolute deviation

    :param xin:
    :return:
    """
    z = np.median(abs(xin - np.median(xin))) / 0.6735

    return z


def generate_mixture(nb_sources=2, dim_observations=1024, nb_observ=2, prop_bern=0.02, s_type=1, cd_a=1,
                     noise_level=120):
    """ Generate mixtures of uniform random variables

    :param nb_sources: Hidden dimensions used for computation
    :param dim_observations: Number of samples in the output
    :param nb_observ: dimensions of the output
    :param prop_bern: For Bernouilli, proportion of different values (nb_diff_values = p * nb_samples)
    :param s_type: Source type =
                0 - Bernouilli Gaussian,
                1 - Gaussian,
                2 - Uniform,
                3 - Approx. sparse,
                4 - SPC
    :param cd_a: Condition number
    :param noise_level:
    :return:
    """

    if nb_observ < nb_sources:
        print('Careful: Less observations than sources')

    a = np.random.randn(nb_observ, nb_sources)
    u_a, s_a, v_a = np.linalg.svd(a)

    s = np.zeros((nb_observ, nb_sources))
    min_dim = min(nb_observ, nb_sources)

    s_a = np.linspace(1 / cd_a, 1, min_dim)
    s[:min_dim, :min_dim] = np.diag(s_a)

    # a = np.dot(u_a[:, 0:hidden_dim], np.dot(np.diag(s_a), v_a[:, 0:hidden_dim].T))
    a = np.dot(u_a, np.dot(s, v_a))

    source = np.zeros((nb_sources, dim_observations))

    if s_type == 0:
        # Generating Bernoulli-Gaussian sources
        k = np.floor(prop_bern * dim_observations)

        for r in range(0, nb_sources):
            u = np.arange(0, dim_observations)
            np.random.shuffle(u)
            source[r, u[0:k]] = np.random.randn(k)

    elif s_type == 1:
        # Gaussian sources
        source = np.random.randn(nb_sources, dim_observations)

    elif s_type == 2:
        # Uniform sources
        source = np.random.rand(nb_sources, dim_observations) - 0.5

    elif s_type == 3:
        # Approx. sparse sources
        source = np.random.randn(nb_sources, dim_observations)
        source = np.power(source, 3)

    elif s_type == 4:
        # SPC sources
        k = np.floor(prop_bern * dim_observations)
        k_c = np.floor(k * 0.3)  # 30% have exactly the same locations

        u = np.arange(0, dim_observations)
        np.random.shuffle(u)
        source[:, u[0:k_c]] = np.random.randn(nb_sources, k_c)

        for r in range(0, nb_sources):
            v = k_c + np.arange(0, dim_observations - k_c)
            df = k - k_c
            np.random.shuffle(v)
            v = v[0:df]
            v = v.astype(np.int64)
            source[r, u[v]] = np.random.randn(df)

    else:
        print('SType takes an unexpected value ...')

    x = np.dot(a, source)

    if noise_level < 120:
        # Add noise
        noise = np.random.randn(nb_observ, dim_observations)
        noise = np.power(10., -noise_level / 20.) * lng.norm(x) / lng.norm(noise) * noise
        x = x + noise

    return x, a, source


def perform_pca(x, n):
    """ Perform a PCA (Principle Component Anlaysis)

    :param x: input data
    :param n: number components to keep
    :return u_y:
    :return s_y:
    """
    # We copy the x matrix
    x_copy = cp.copy(x)

    # We set each row to a zero mean
    x_mean_rows = np.dot(np.diag(np.mean(x_copy, axis=1)), np.ones(np.shape(x_copy)))
    x_zero_mean = x_copy - x_mean_rows

    # We compute the covariance matrix, considering that cov_x_zm ~= x_copy x_copy.T / n
    cov_x_zm = np.dot(x_zero_mean, x_zero_mean.T)
    # The SVD decomposition of the covariance matrix
    u_y, d_y, v_y = np.linalg.svd(cov_x_zm)

    # We get the n first column of U
    u_y = u_y[:, 0:n]
    d_y = d_y[0:n]
    # We compute S = diag(1/sqrt(diag_y)) * U * X
    s_y = np.dot(np.diag(1. / (1e-12 + np.sqrt(d_y))), np.dot(u_y.T, x))

    return u_y, s_y, d_y


def perform_ilc(x, colcmb):
    """ Code to perform ILC

    :param x:
    :param colcmb:
    :return:
    """
    # Remove the mean value
    x_copy = cp.copy(x)
    x_mean_col = np.dot(np.diag(np.mean(x_copy, axis=1)), np.ones(np.shape(x_copy)))
    x_zero_mean = x_copy - x_mean_col

    inv_rx = lng.inv(np.dot(x_zero_mean, x_zero_mean.T))

    c = np.reshape(colcmb, (len(colcmb), 1))

    w = 1. / np.dot(c.T, np.dot(inv_rx, c)) * np.dot(c.T, inv_rx)

    s = np.dot(w, x)

    return s, w


def perform_fast_ica(x, n_sources=None, do_whitening=False):
    """ Code to perform FastICA

    :param x: data - has to be whitened if do_whitening==False
    :param n_sources:
    :param do_whitening: bool: have the data to be whiten ?
    :return a_est: A: The estimated mixing matrix
    :return sources_est: S: The estimated sources
    """

    x_copy = cp.copy(x)
    x_copy = x_copy.T

    fast_ica = FastICA(n_components=n_sources, whiten=do_whitening)

    sources_est = fast_ica.fit(x_copy).transform(x_copy).T  # Get the estimated sources
    a_est = fast_ica.mixing_  # Get estimated mixing matrix

    return a_est, sources_est


def perform_gmca(x, nb_sources, nmax=100, mints=0.5, maxts=0):
    """ Code to perform basic GMCA

    :param x:
    :param nb_sources:
    :param nmax:
    :param mints:
    :param maxts:
    :return:
    """
    z = np.shape(x)
    # t = z[1]
    m = z[0]

    a = np.random.randn(m, nb_sources)
    for r in range(0, nb_sources):
        a[:, r] = a[:, r] / lng.norm(a[:, r])

    r_a = np.dot(a.T, a)
    s = np.dot(np.diag(1. / np.diag(r_a)), np.dot(a.T, x))

    ts = 0
    if maxts == 0:
        for r in range(1, nb_sources):
            maxts = np.max([np.max(abs(s[r, :])) / mad(s[r, :]), ts])

    vts = np.exp((np.log(maxts) - np.log(mints)) * np.linspace(0, 1, nmax)[::-1] + np.log(mints))
    vepsilon = np.power(10, 4 * np.linspace(0, 1, nmax)[::-1] - 5)

    p_inv_a = np.zeros(a.shape)

    for nit in range(0, nmax):

        # Estimate the sources for fixed a

        epsilon = vepsilon[nit]
        ts = vts[nit]

        r_a = np.dot(a.T, a)
        m_r_a = lng.norm(r_a, 2)

        r_a = r_a + epsilon * m_r_a * np.identity(nb_sources)
        p_inv_a = np.dot(lng.inv(r_a), a.T)
        s = np.dot(p_inv_a, x)

        for ns in range(0, nb_sources):
            temp = s[ns, :]
            thrd = ts * mad(temp)
            s[ns, (abs(temp) < thrd)] = 0

        # Estimate the mixing matrix for fixed sources s

        vs = np.sqrt(np.sum(s * s, axis=1))

        ind_a = np.where(vs > 1e-6)
        ind_a = ind_a[0]

        nactive = len(ind_a)

        if nactive > 1:

            temp = s[ind_a, :]
            r_p = np.dot(temp, temp.T)
            m_r_p = lng.norm(r_p, 2)
            r_p = r_p + epsilon * m_r_p * np.identity(nactive)
            p_inv_s = np.dot(temp.T, lng.inv(r_p))
            a[:, ind_a] = np.dot(x, p_inv_s)

            for ns in ind_a:
                a[:, ns] = a[:, ns] / float(lng.norm(a[:, ns] + 1e-6))
                if lng.norm(a[:, ns]) < 1e-6:
                    a[:, ns] = np.random.randn(m)

    return a, s, p_inv_a


def perform_gmca_planck(x, n, nmax=100, mints=3, colcmb=None, maxts=7):
    """ Code to perform basic GMCA Planck

    :param x:
    :param n:
    :param nmax:
    :param mints:
    :param colcmb:
    :param maxts:
    :return:
    """

    z = np.shape(x)
    # t = z[1]
    m = z[0]

    a = np.random.randn(m, n)
    for r in range(0, n):
        a[:, r] = a[:, r] / lng.norm(a[:, r])

    if colcmb is not None:  # Do not update the first column
        vc = np.reshape(colcmb, (len(colcmb)))
        a[:, 0] = vc

    s = np.dot(a.T, x)

    ts = maxts
    dts = (ts - mints) / float(nmax - 1.)

    p_inv_a = np.zeros(a.shape)

    for nit in range(0, nmax):

        # Estimate the sources for fixed a

        r_a = np.dot(a.T, a)
        p_inv_a = np.dot(lng.inv(r_a), a.T)
        s = np.dot(p_inv_a, x)

        for ns in range(0, n):
            temp = s[ns, :]
            thrd = ts * mad(temp)
            s[ns, (abs(temp) < thrd)] = 0

        # Estimate the mixing matrix for fixed sources s

        vs = np.sqrt(np.sum(s * s, axis=1))

        if colcmb is not None:  # Do not update the first column
            vs[0] = 0

        ind_a = np.where(vs > 1e-6)
        ind_a = ind_a[0]

        nactive = len(ind_a)

        if nactive > 1:

            temp = s[ind_a, :]
            r_p = np.dot(temp, temp.T)
            p_inv_s = np.dot(temp.T, lng.inv(r_p))
            a[:, ind_a] = np.dot(x, p_inv_s)

            for ns in ind_a:
                a[:, ns] = a[:, ns] / float(lng.norm(a[:, ns] + 1e-6))
                if lng.norm(a[:, ns]) < 1e-6:
                    a[:, ns] = np.random.randn(m)

        # Update the threshold

        ts = ts - dts

    return a, s, p_inv_a


def eval_bss(a_gnd_truth, _, a_est, sources_est):
    """ CODE TO COMPUTE THE MIXING MATRIX CRITERION (AND SOLVES THE PERMUTATION INDETERMINACY)

    :param a_gnd_truth:
    :param _: Was before s0, but not used
    :param a_est:
    :param sources_est:
    :return:
    """

    diff = np.dot(lng.inv(np.dot(a_est.T, a_est)), np.dot(a_est.T, a_gnd_truth))

    z = np.shape(a_est)

    for ns in range(0, z[1]):
        diff[ns, :] = abs(diff[ns, :]) / max(abs(diff[ns, :]))

    q = np.ones(z)
    sq = np.ones(np.shape(sources_est))

    for ns in range(0, z[1]):
        q[:, np.nanargmax(diff[ns, :])] = a_est[:, ns]
        sq[np.nanargmax(diff[ns, :]), :] = sources_est[ns, :]

    diff = np.dot(lng.inv(np.dot(q.T, q)), np.dot(q.T, a_gnd_truth))

    for ns in range(0, z[1]):
        diff[ns, :] = abs(diff[ns, :]) / max(abs(diff[ns, :]))

    p = (np.sum(diff) - z[1]) / (z[1] * (z[1] - 1))

    return p


def cube2mat(x, sdim=0):
    """ Data cube to matrix conversion

    :param x:
    :param sdim:
    :return:
    """
    y = cp.copy(x)
    z = np.shape(y)

    if sdim == 0:
        m = np.zeros((z[0], z[1] * z[2]))
        for r in range(0, z[0]):
            m[r, :] = np.reshape(y[r, :, :], (1, z[1] * z[2]))

    if sdim == 1:
        m = np.zeros((z[1], z[0] * z[2]))
        for r in range(0, z[1]):
            m[r, :] = np.reshape(y[:, r, :], (1, z[0] * z[2]))

    else:
        # if sdim == 2:
        m = np.zeros((z[2], z[0] * z[1]))
        for r in range(0, z[2]):
            m[r, :] = np.reshape(y[:, :, r], (1, z[0] * z[1]))

    return m


def mat2cube(m, nx, ny, sdim=0):
    """ Matric to data cube conversion

    :param m:
    :param nx:
    :param ny:
    :param sdim:
    :return:
    """
    y = cp.copy(m)
    z = np.shape(y)

    if sdim == 0:
        x = np.zeros((z[0], nx, ny))
        for r in range(0, z[0]):
            x[r, :, :] = np.reshape(y[r, :], (nx, ny))

    if sdim == 1:
        x = np.zeros((nx, z[0], ny))
        for r in range(0, z[0]):
            x[:, r, :] = np.reshape(y[r, :], (nx, ny))

    else:
        # sdim == 2
        x = np.zeros((nx, ny, z[0]))
        for r in range(0, z[0]):
            x[:, :, r] = np.reshape(y[r, :], (nx, ny))

    return x
