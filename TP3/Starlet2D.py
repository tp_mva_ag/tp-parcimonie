"""
Created on Wed Jan  4 16:54:28 2017

@author: jbobin
"""

import numpy as np
from copy import deepcopy as dp
import copy as cp


def length(x: np.ndarray):
    len_x = np.max(np.shape(x))
    return len_x


def filter_1d(x_in: np.ndarray, h: np.ndarray, boption=3):
    """ 1D Convolution

    :param x_in:
    :param h:
    :param boption:
    :return:
    """

    x = np.squeeze(cp.copy(x_in))
    n = length(x)
    m = length(h)
    y = cp.copy(x)

    z = np.zeros(1, m)

    m2 = np.int(np.floor(m / 2))

    for r in range(m2):

        if boption == 1:  # --- zero padding
            z = np.concatenate([np.zeros(m - r - m2 - 1), x[0:r + m2 + 1]], axis=0)

        if boption == 2:  # --- periodicity
            z = np.concatenate([x[n - (m - (r + m2)) + 1:n], x[0:r + m2 + 1]], axis=0)

        if boption == 3:  # --- mirror
            u = x[0:m - (r + m2) - 1]
            u = u[::-1]
            z = np.concatenate([u, x[0:r + m2 + 1]], axis=0)

        y[r] = np.sum(z * h)

    a = np.arange(np.int(m2), np.int(n - m + m2), 1)

    for r in a:
        y[r] = np.sum(h * x[r - m2:m + r - m2])

    a = np.arange(np.int(n - m + m2 + 1) - 1, n, 1)

    for r in a:

        if boption == 1:  # --- zero padding
            z = np.concatenate([x[r - m2:n], np.zeros(m - (n - r) - m2)], axis=0)

        if boption == 2:  # --- periodicity
            z = np.concatenate([x[r - m2:n], x[0:m - (n - r) - m2]], axis=0)

        if boption == 3:  # --- mirror
            u = x[n - (m - (n - r) - m2 - 1) - 1:n]
            u = u[::-1]
            z = np.concatenate([x[r - m2:n], u], axis=0)

        y[r] = np.sum(z * h)

    return y


def apply_h1(x: np.ndarray, h: np.ndarray, scale=1, boption=3):
    """ 1D convolution with the "a trous" algorithm

    :param x:
    :param h:
    :param scale:
    :param boption:
    :return:
    """
    m = length(h)

    if scale > 1:
        p = (m - 1) * np.power(2, (scale - 1)) + 1
        g = np.zeros(p)
        z = np.linspace(0, m - 1, m) * np.power(2, (scale - 1))
        g[z.astype(int)] = h

    else:
        g = h

    y = filter_1d(x, g, boption)

    return y


def starlet_forward_2d(x=0, h=np.array([0.0625, 0.25, 0.375, 0.25, 0.0625]), j=1, boption=3):
    """ 2D "a trous" algorithm

    :param x:
    :param h:
    :param j:
    :param boption:
    :return:
    """

    nx = np.shape(x)
    # c = np.zeros((nx[0], nx[1]), dtype=complex)
    w = np.zeros((nx[0], nx[1], j))

    c = cp.copy(x)
    cnew = cp.copy(x)

    for scale in range(j):

        for r in range(nx[0]):
            cnew[r, :] = apply_h1(c[r, :], h, scale, boption)

        for r in range(nx[1]):
            cnew[:, r] = apply_h1(cnew[:, r], h, scale, boption)

        w[:, :, scale] = c - cnew

        c = cp.copy(cnew)

    return c, w


def starlet_forward_1d(x: np.ndarray, h=np.array([0.0625, 0.25, 0.375, 0.25, 0.0625]), j=1, boption=3):
    # c = np.zeros((len(x),), dtype=complex)
    w = np.zeros((len(x), j))

    c = cp.copy(x)
    # cnew = cp.copy(x)

    for scale in range(j):
        cnew = apply_h1(c, h, scale, boption)

        w[:, scale] = c - cnew

        c = cp.copy(cnew)

    return c, w


def starlet_backward_1d(c, w):
    return c + np.sum(w, axis=1)


def starlet_backward_2d(c, w):
    return c + np.sum(w, axis=2)


def mad(z):
    return np.median(abs(z - np.median(z))) / 0.6735


def starlet_filter_1d(x: np.ndarray, kmad=3, h=np.array([0.0625, 0.25, 0.375, 0.25, 0.0625]), j=1, boption=3, len0=0,
                      perscale=0):
    c, w = starlet_forward_1d(x, h=h, j=j, boption=boption)

    thrd = kmad * mad(w[:, 0])  # Added line
    for r in range(j):

        if perscale:
            thrd = kmad * mad(w[:, r])
        # else:
        #     if r == 0:
        #         thrd = kmad * mad(w[:, r])  # Estimate the threshold in the first scale only

        if len0:
            w[:, r] = w[:, r] * (abs(w[:, r]) > thrd)
        else:
            w[:, r] = (w[:, r] - thrd * np.sign(w[:, r])) * (abs(w[:, r]) > thrd)

    return starlet_backward_1d(c, w)


def starlet_filter_2d(x=0, kmad=3, h=np.array([0.0625, 0.25, 0.375, 0.25, 0.0625]), j=1, boption=3, len0=0, perscale=0):
    c, w = starlet_forward_2d(x, h=h, j=j, boption=boption)

    thrd = kmad * mad(w[:, :, 0])  # Added line
    for r in range(j):

        if perscale:
            thrd = kmad * mad(w[:, :, r])
        # else:
        #     if r == 0:
        #         thrd = kmad * mad(w[:, :, r])  # Estimate the threshold in the first scale only

        if len0:
            w[:, :, r] = w[:, :, r] * (abs(w[:, :, r]) > thrd)
        else:
            w[:, :, r] = (w[:, :, r] - thrd * np.sign(w[:, :, r])) * (abs(w[:, :, r]) > thrd)

    return starlet_backward_2d(c, w)


def fbs_inpainting(b, mask, kmad=3, j=3, nmax=100, len0=0, perscale=1):
    x = 0. * dp(b)

    alpha = 1  # path length

    for r in range(nmax):
        # Compute the gradient

        delta = mask * (mask * x - b)

        # Gradient descent

        x_half = x - alpha * delta

        # Sparsity constraint

        xp = starlet_filter_2d(x=x_half, kmad=kmad, j=j, boption=3, len0=len0, perscale=perscale)

        x = dp(xp)

    return x
