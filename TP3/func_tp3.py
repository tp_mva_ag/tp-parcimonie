from scipy.io import loadmat
import matplotlib.pyplot as plt

# from TP3.Starlet2D import *
from TP3.pyBSS import *


def whitening(x, a=None):
    """ Determine the whitenning of the x matrix

    Sources:
    https://theclevermachine.wordpress.com/2013/03/30/the-statistical-whitening-transform/
    http://ufldl.stanford.edu/wiki/index.php/Implementing_PCA/Whitening

    :param a:
    :param x: data to whiten
    :return:
    """

    # Parameters
    nb_samples = x.shape[0]
    n = nb_samples

    # Centering the columns (ie the variables)
    # x_mean = x.mean(axis=0)
    # x_zero_mean = x - x_mean[np.newaxis, :]

    # We get the empirical covariance matrix
    cov = np.dot(x, x.T) / (n - 1)

    # We could also use svd, but this seems slower
    p, sigma, _ = np.linalg.svd(cov)
    # sigma, p = np.linalg.eig(cov)
    # sigma = np.real(sigma)
    # p = np.real(p)

    # This matrix correspond to D ** (-1/2)
    sigma_1_2 = np.diag(sigma ** (-1 / 2))

    pi = sigma_1_2.dot(p.T)

    x_w = pi.dot(x)
    if a is not None:
        a_w = pi.dot(a)
        return x_w, a_w

    else:
        return x_w
    # We return the whiten vector


def best_rank(x, n):
    """ Provides the best rank-n approximation of x, according to the Euclidean norm

    :param x: data to approximate
    :param n: rank
    :return x[best_idx]: approximation. Not a copy, be careful
    """

    # nb_samples = x.shape[1]
    # t = nb_samples

    # Parameters
    nb_obs = x.shape[0]
    m = nb_obs

    x_w = whitening(x)

    # We will find the dimensions of highest norm and keep only them.
    # This would then represent the best rank-n approximation of x according to the Euclidean norm
    list_norms = [np.linalg.norm(x_w[i]) for i in range(m)]

    # We get the sorted indexes and keep only the n bigger, and finally inverse it to get the biggest one first
    biggest_idx = np.argsort(list_norms)[-n:][::-1]

    # Finally, we only keep the n best dimensions, based on the previous indices
    return x[biggest_idx]


def normalize(x):
    vmin = np.min(x)
    vmax = np.max(x)

    return (x - vmin) / (vmax - vmin) - 0.5


def main_pca():
    mixt, a, sources = generate_mixture(dim_observations=4096, cd_a=5, nb_sources=5)

    u_mixt, s_mixt, d_mixt = perform_pca(mixt, 2)

    print(u_mixt)

    mean_x = mixt[0].mean()
    mean_y = mixt[1].mean()

    plt.subplot(121)
    plt.scatter(mixt[0], mixt[1])
    for i in range(u_mixt.shape[0]):
        div = np.sqrt(d_mixt[i]) / 30
        plt.plot(mean_x + [0, u_mixt[i, 0] * div], mean_y + [0, u_mixt[i, 1] * div], 'r')
    plt.axis('equal')

    plt.subplot(122)
    plt.scatter(s_mixt[0], s_mixt[1])
    plt.axis('equal')

    plt.show()


def main_ica():
    """ Main function for testing ICA functions

    """

    cd_a_list = [1, 2, 10, 50, 100]
    nb_mixt_per_cd = 2

    # We try all the condition
    for idx_cd, condition in enumerate(cd_a_list):

        # For each one, we try multiple mixtures
        for i in range(nb_mixt_per_cd):
            # s_type = 2  for Uniform sources
            x, a, sources = generate_mixture(dim_observations=256, cd_a=condition, nb_sources=2, s_type=2)

            # We whiten the data and send them to the algorithm
            mixt_w, a_w = whitening(x, a)
            a_est, sources_est = perform_fast_ica(mixt_w, do_whitening=False)

            sources_norm = [normalize(sources[0]), normalize(sources[1])]
            sources_est_norm = [normalize(sources_est[0]), normalize(sources_est[1])]

            # Plotting the results
            plt.subplot(nb_mixt_per_cd, len(cd_a_list), (len(cd_a_list) * i) + idx_cd + 1)

            plt.scatter(sources_norm[0], sources_norm[1], c='blue', label='Orig sources',
                        alpha=0.3, edgecolors='none')
            plt.scatter(sources_est_norm[0], sources_est_norm[1], c='red', label='Orig sources',
                        alpha=0.3, edgecolors='none')

            plt.title('cd=%d BSS=%.3f' % (condition, eval_bss(a, sources, a_est, sources_est)))
            plt.legend()
            plt.grid(True)

    plt.show()


def main_chandra_ica():
    print('Loading mat')
    chandra = loadmat('TP3/Data/chandra.mat')

    a = chandra['A0']
    sources = chandra['S0']
    x = a.dot(sources)

    # m = 107   # Nb samples
    # n = 8     # Nb sources
    # N = 7396  # Nb dimensions

    # print('Whitening of X, usually takes up to XXXs')
    # start_time = time()
    x_w, a_w = whitening(x, a)
    # print('End of the whitening in %.3f' % (time() - start_time))

    a_est, sources_est = perform_fast_ica(x_w, do_whitening=False, n_sources=8)
    # a_est, sources_est = perform_fast_ica(x, whiten=True, n_components=8)

    print('Mean absolute error between x and estimated : %.3f' % np.mean(np.abs(x_w - a_est.dot(sources_est))))

    for idx in range(4):
        source = sources[idx]
        source_e = sources_est[idx]

        plt.subplot(2, 4, idx + 1)

        # bins = np.linspace(min(source.min(), source_e.min()),
        #                    max(source.max(), source_e.max()),
        #                    100)

        plt.hist(source, 100, log=True, alpha=0.5, color='blue', label='Orig source')

        plt.subplot(2, 4, idx + 5)
        plt.hist(source_e, 100, log=True, alpha=0.5, color='red', label='Est source')

        plt.title('Source n#%d' % idx)
        plt.legend()

    plt.show()
    # print('BSS = %s' % eval_bss(a, sources, a_est, sources_est))


def main_gmca():
    """ Part 3 : estimation of sources thanks to GMCA

    """
    # Mixture : 2 uniform sources
    x, a, sources = generate_mixture(dim_observations=2048, nb_sources=2, s_type=2)

    # Estimation
    a_est, sources_est, p_inv_a = perform_gmca(x, 2)

    # Plotting in a 2D space
    plt.figure()
    plt.scatter(sources[0], sources[1], c='blue', label='Orig sources',
                alpha=0.3, edgecolors='none')
    plt.scatter(sources_est[0], sources_est[1], c='red', label='Estimated sources',
                alpha=0.3, edgecolors='none')
    plt.legend()

    # Plotting the histograms of the sources, more representatives
    plt.figure()
    plt.subplot(121)
    plt.hist(sources[0], 100, log=True, alpha=0.5, color='blue', label='Orig source')
    plt.hist(sources_est[0], 100, log=True, alpha=0.5, color='red', label='Estimated source')
    plt.legend()

    plt.subplot(122)
    plt.hist(sources[1], 100, log=True, alpha=0.5, color='blue', label='Orig source')
    plt.hist(sources_est[1], 100, log=True, alpha=0.5, color='red', label='Estimated source')
    plt.legend()

    plt.show()


# if __name__ == '__main__':
    # main_ica()
    # main_chandra_ica()
    # main_gmca()
