from TP3.func_tp3 import *


def plot_mixt_2_obs(x_1, x_2, title_1='Original Mixture', title_2='Whiten Mixture', block=True):
    plt.figure()
    plt.subplot(121)
    plt.scatter(x_1[0], x_1[1], c='blue', label='Orig X', alpha=0.3, edgecolors='none')
    plt.title(title_1)
    plt.axis('equal')

    plt.subplot(122)
    plt.scatter(x_2[0], x_2[1], c='red', label='X whiten', alpha=0.3, edgecolors='none')
    plt.title(title_2)
    plt.axis('equal')

    plt.show(block=block)


def plot_sources_hist(sources, sources_est, block=True, nb_bins=100):
    nb_sources = sources.shape[0]
    sources_n = normalize(sources)
    for idx in range(nb_sources):
        source_n = sources_n[idx]

        plt.subplot(2, nb_sources, idx + 1)
        plt.hist(source_n, nb_bins, log=True, alpha=0.5, color='blue', label='Orig source')

        plt.title('Source n#%d' % idx)
        plt.legend()

    nb_sources_est = sources_est.shape[0]
    sources_est_n = normalize(sources_est)
    for idx in range(nb_sources_est):
        source_est_n = sources_est_n[idx]

        plt.subplot(2, nb_sources_est, nb_sources_est + idx + 1)
        plt.hist(source_est_n, nb_bins, log=True, alpha=0.5, color='red', label='Est source')

        plt.title('Source est n#%d' % idx)
        plt.legend()

    plt.show(block=block)


def test_whiten():
    x, a, sources = generate_mixture(s_type=1, cd_a=4)

    x_n = normalize(x)
    x_white = whitening(x)
    x_w_n = normalize(x_white)

    plot_mixt_2_obs(x_n, x_w_n)


def test_pca(nb_sources=4, nb_observ=4):
    x, a, sources = generate_mixture(nb_observ=nb_observ, nb_sources=nb_sources, s_type=1, cd_a=4)

    u, sources_est, d = perform_pca(x, nb_sources)

    if nb_sources == 2:
        plot_mixt_2_obs(sources, sources_est, "Original sources", "Estimated sources")
    else:
        plot_sources_hist(sources, sources_est)


def test_pca_unif(nb_sources=4, nb_observ=4):
    x, a, sources = generate_mixture(nb_observ=nb_observ, nb_sources=nb_sources, s_type=2, cd_a=4)

    u, sources_est, d = perform_pca(x, nb_sources)

    if nb_sources == 2:
        plot_mixt_2_obs(sources, sources_est, "Original sources", "Estimated sources")
    else:
        plot_sources_hist(sources, sources_est)


def test_fast_ica(cd_a=1, block=True, plot=True):
    nb_observ = 2
    nb_sources = 2
    x, a, sources = generate_mixture(dim_observations=4096, nb_observ=nb_observ, nb_sources=nb_sources, s_type=2,
                                     cd_a=cd_a)

    x_w, a_w = whitening(x, a)
    a_est, sources_est = perform_fast_ica(x_w, do_whitening=False)
    x_w_est = a_est.dot(sources_est)

    if plot:
        print('Mean squared error between mixing and estimated mixing = %.3f' % np.mean(np.square(x_w - x_w_est)))

        plot_sources_hist(sources, sources_est, block=block)
        plot_mixt_2_obs(sources, sources_est, block=block, title_2="Estimated sources", title_1="Original Sources")

    return a, sources, a_est, sources_est


def test_mult_fast_ica(cd_list, nb_try=5):
    bss_list = []
    for cd in cd_list:
        plt.figure()
        bss = 0

        a, _, a_est, sources_est = test_fast_ica(cd_a=cd, block=False, plot=True)
        bss += eval_bss(a, _, a_est, sources_est)
        for _ in range(nb_try - 1):
            a, _, a_est, sources_est = test_fast_ica(cd_a=cd, block=False, plot=False)
            bss += eval_bss(a, _, a_est, sources_est)

        plt.suptitle('Cd_a = %d - BSS = %.3f' % (cd, bss/nb_try))
        bss_list.append(bss/nb_try)

    plt.figure()
    plt.plot(cd_list, bss_list)
    plt.xlabel('cd_a')
    plt.ylabel('BSS (to minimize)')
    plt.title('Evolution of BSS depending on Condition parameter of A')


def test_fast_ica_chandra(own_whitening=True):
    print('Loading mat')
    chandra = loadmat('TP3/Data/chandra.mat')

    a = chandra['A0']
    sources = chandra['S0']
    nb_sources = sources.shape[0]
    x = a.dot(sources)

    x_w, a_w = whitening(x, a)
    if own_whitening:
        x_w = best_rank(x_w, nb_sources)

        a_est, sources_est = perform_fast_ica(x_w, do_whitening=False)

    else:
        a_est, sources_est = perform_fast_ica(x, nb_sources, do_whitening=True)

    x_w_est = a_est.dot(sources_est)

    print('Mean squared error between mixing and estimated mixing = %.3f' % np.mean(np.square(x_w - x_w_est)))
    print('BSS criterion = %.3f' % eval_bss(a, _, a_est, sources_est))

    plot_sources_hist(sources, sources_est, block=False)


def test_gmca_unif(nb_observ=2, nb_sources=2, block=False):
    x, a, sources = generate_mixture(nb_observ=nb_observ, nb_sources=nb_sources, s_type=2)

    x_w, a_w = whitening(x, a)
    a_est, sources_est, _ = perform_gmca(x_w, nb_sources)
    x_w_est = a_est.dot(sources_est)

    print('Mean squared error between mixing and estimated mixing = %.3f' % np.mean(np.square(x_w - x_w_est)))

    plot_sources_hist(sources, sources_est, block=block, nb_bins=500)
    plot_mixt_2_obs(sources, sources_est, block=block, title_2="Estimated sources", title_1="Original Sources")


def test_gmca_sparse(nb_observ=2, nb_sources=2, block=False, noise=120):
    # Approximatively sparse sources
    x, a, sources = generate_mixture(nb_observ=nb_observ, nb_sources=nb_sources, s_type=3, noise_level=noise)

    x_w, a_w = whitening(x, a)
    a_est, sources_est, _ = perform_gmca(x_w, nb_sources)
    x_w_est = a_est.dot(sources_est)

    print('Mean squared error between mixing and estimated mixing = %.3f' % np.mean(np.square(x_w - x_w_est)))

    plot_sources_hist(sources, sources_est, block=block, nb_bins=500)
    plot_mixt_2_obs(sources, sources_est, block=block, title_2="Estimated sources", title_1="Original Sources")


# noinspection PyUnboundLocalVariable
def test_gmca_ica_mult_noise(nb_observ=2, nb_sources=2, block=False, noise_list=None, nb_essais=1):
    if noise_list is None:
        noise_list = [40, 30, 20, 10]

    for noise in noise_list:
        crit_gmca = 0
        crit_ica = 0
        for _ in range(nb_essais):
            # Approximatively sparse sources
            x, a, sources = generate_mixture(nb_observ=nb_observ, nb_sources=nb_sources, s_type=3, noise_level=noise)

            x_w, a_w = whitening(x, a)
            a_gmca, sources_gmca, _ = perform_gmca(x_w, nb_sources)
            a_ica, sources_ica = perform_fast_ica(x_w, do_whitening=False)

            crit_gmca += eval_bss(a, _, a_gmca, sources_gmca)
            crit_ica += eval_bss(a, _, a_ica, sources_ica)

        x_w_gmca = a_gmca.dot(sources_gmca)
        x_w_ica = a_gmca.dot(sources_ica)

        crit_gmca /= nb_essais
        crit_gmca /= nb_essais

        print('----- Noise = %d -----' % noise)

        mse_gmca = float(np.mean(np.square(x_w - x_w_gmca)))
        mse_ica = float(np.mean(np.square(x_w - x_w_ica)))

        print('MSE between mixing and estimated mixing:\n'
              '\tGMCA : \t\t%.3f\n'
              '\tFast ICA : \t%.3f\n'
              'Mixing Matrix Criterion:\n'
              '\tGMCA : \t\t%.3f\n'
              '\tFast ICA : \t%.3f\n'
              % (mse_gmca, mse_ica, crit_gmca, crit_ica))

        plot_mixt_2_obs(sources, sources_gmca, block=block,
                        title_1="Original Sources - SNR = %d" % noise, title_2="GMCA - SNR = %d" % noise)
        plot_mixt_2_obs(sources, sources_ica, block=block,
                        title_1="Original Sources - SNR = %d" % noise, title_2="Fast ICA - SNR = %d" % noise)


if __name__ == '__main__':
    # test_whiten()
    # test_pca()
    # PCA seems to work in this case
    # test_mult_fast_ica([1, 2, 10, 50, 100])
    # test_fast_ica_chandra(own_whitening=True)
    # test_gmca_unif()
    # test_gmca_sparse()
    # test_gmca_sparse(noise=10)
    # test_gmca_sparse(noise=20)
    test_gmca_ica_mult_noise(nb_essais=5)

    plt.show()
